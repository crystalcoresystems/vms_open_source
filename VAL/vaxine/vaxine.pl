# Vax-like instruction engine

@memory = get_data();
$l = 0;
$q = 0;
$fh = shift(@memory);
my $ip = 0;
while () {
        $ip = execute($ip);
        $ip == -1 and last;
}
exit;

sub execute {#../
        my $ip = shift @_;
        my @code = split //, @memory[$ip];
        my $ins = @code[1];
        $ip++;
        if ($ins == 0) {mvi(@code)}
        elsif ($ins == 1) {mov(@code)}
        elsif ($ins == 2) {add(@code)}
        elsif ($ins == 3) {min(@code)}
        elsif ($ins == 4) {com(@code)}
        elsif ($ins == 5) {$ip = blt($ip, @code)}
        elsif ($ins == 6) {$ip = beq($ip, @code)}
        elsif ($ins == 7) {rdl(@code)}
        elsif ($ins == 8) {wrl(@code)}
        else {$ip = -1}

        return $ip;
}#/..

sub mvi {#../
        my $mode = @_[2]%4;
        my ($D, $B) = mode_AB($mode, @_);
        my $A = @_[3]*10 + @_[4];

        @_[0] eq "-" and $A *= -1;
        @memory[$B] = int2str($A); 
}#/..

sub mov {#../
        my $mode = @_[2]%4;
        my ($A, $B) = mode_AB($mode, @_);
        @memory[$B] = @memory[$A];
}#/..

sub add {#../
        my $mode = @_[2]%4;
        my ($A, $B) = mode_AB($mode, @_);
        my $x = int(@memory[$A]) + int(@memory[$B]);
        $x < -999999 and $x = ($x%-1000000) * -1;
        $x > 999999 and $x = ($x%1000000) * -1;
        @memory[$B] = int2str($x);
}#/..

sub min {# (sub) ../
        my $mode = @_[2]%4;
        my ($A, $B) = mode_AB($mode, @_);
        my $x = int(@memory[$B]) - int(@memory[$A]);
        $x < -999999 and $x = ($x%-1000000) * -1;
        $x > 999999 and $x = ($x%1000000) * -1;
        @memory[$B] = int2str($x);
}#/..

sub com {# (cmp) ../
        my $mode = @_[2]%4;
        my ($A, $B) = mode_AB($mode, @_);
        my $a, $b;
        my @x, @y;

        $l = int(@memory[$A]) < int(@memory[$B]) ? 1 : 0;
        $q = int(@memory[$A]) == int(@memory[$B]) ? 1 : 0;
        if (int(@memory[$A] == 0) && int(@memory[$B]) == 0) {
                @x = split //, @memory[$A];
                @y = split //, @memory[$B];
                $a = shift @x;
                $b = shift @y;
                if ($a eq "-" && $b eq "+") {
                        $l = 1;
                }
        }
}#/..

sub blt {#../
        my $ip = shift;
        my $mode = @_[2]%4;
        my ($A, $B) = mode_AB($mode, @_);
        return $l ? $B : $ip;
}#/..

sub beq {#../
        my $ip = shift;
        my $mode = @_[2]%4;
        my ($A, $B) = mode_AB($mode, @_);
        return $q ? $B : $ip;
}#/..

sub rdl {#../
        my $mode = @_[2]%4;
        my ($D, $B) = mode_AB($mode, @_);
        my $A = @_[3]*10 + @_[4];
        $A == 0 and return;
        my $oa = $A;
        while(<$fh>) {
                chomp($_);
                my $data = $_;
                $data = cut_comment($data);
                length($data) < 7 and next;
                @memory[ ($B + $oa-$A)%100 ] = $data;
                $A--;
                $A == 0 and return;
        }
}#/..

sub cut_comment {#../
        my $i = 0;
        my $string = shift;
        foreach (split //, $string) {
                $_ eq ";" and last;
                $i++;
        }
        return substr($string, 0 ,$i);
}#/..

sub wrl {#../
        my $mode = @_[2]%4;
        my ($D, $B) = mode_AB($mode, @_);
        my $A = @_[3]*10 + @_[4];
        for(0..$A-1) {
                print @memory[($_ + $B)%100], "\n";
        }
}#/..

sub int2str {#../
        my $x = shift;
        my $a = $x < 0 ? "-" : "+";
        my $digits = 1;
        my $z = 10;
        while(int($x/$z)) {
                $digits++;
                $z *= 10;
        }
        $x = abs($x)%1000000;
        $a = $a . "0" x (6-$digits);
        return $a . $x;
}#/..

sub mode_AB { #../
        my $to_store; my $store_at;
        my $ind_a; my $ind_b; my @a; 

        my $mode = shift;
        if ($mode == 0 ) {
                $to_store = @_[3]*10 + @_[4];
                $store_at = @_[5]*10 + @_[6];
                return ($to_store, $store_at);
        }
        elsif ($mode == 1) {
                $to_store = @_[3]*10 + @_[4];
                $ind_b = @_[5]*10 + @_[6]; 
                @a = split //, @a[$ind_b];
                $store_at = @a[5]*10 + @a[6];
        }
        elsif ($mode == 2) {
                $ind_a = @_[3]*10 + @_[4];
                @a = split //, @a[$ind_a];
                $to_store = @a[3]*10 + @a[4];
                $store_at = @_[5]*10 + @_[6];
        }
        elsif ($mode == 3) {
                $ind_a = @_[3]*10 + @_[4];
                @a = split //, @a[$ind_a];
                $to_store = @a[3]*10 + @a[4];
                $ind_b = @_[5]*10 + @_[6];
                @a = split //, @a[$ind_b];
                $store_at = @a[5]*10 + @a[6];
        }
        return ($to_store, $store_at);
}#/..

sub get_data {#../
        my $i = 1;
        print "\ndatafile: ";
        chomp(my $datafile = <>);
        open my $fh, '<', $datafile or die "fnf\n";
        print "\n\n";
        my @mem = ("+900000") x 100;

        while(<$fh>) {
                chomp($_);
                $_ == -1 and last;
                $_ = cut_comment($_);
                length($_) < 10 and next;
                my ($loc, $data) = split / /, $_;
                @mem[$loc] = $data;
        }
        return ($fh, @mem);
}#/..
