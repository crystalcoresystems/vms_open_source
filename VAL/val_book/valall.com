$!      VALALL.COM
$!
$! Check for at least one program name.
$!
$ if p1 .nes. "" then goto OKAY
$ write sys$output " "
$ write sys$output "No File Name(s) Given. Try Again."
$ goto EXIT
$!
$ OKAY:
$       pname = f$parse(p1,,,"NAME")
$       if vfile .nes "" then write sys$output -
                "Input data (if any) must be in file "'pname'".DAT ."
$!
$       comma := ","
$!
$! Check for data file.
$!
$       dat := 'pname'.DAT;*
$       dat := 'f$search(DAT)
$       write sys$output " "
$!
$! Skip assembly and linking if run only
$!
$       if vrunonly .eqs. "YES" then goto RUNONLY
$!
$! Consolidate Directory
$!
$       write sys$output "*** Consolidating Directory ***"
$       purge/keep=2
$       if f$search("*.LIS;*") .nes. "" then delete *.LIS;*
$       if f$search("*.EXE;*") .nes. "" then delete *.EXE;*
$       if f$search("*.OUT;*") .nes. "" then delete *.OUT;*
$       if f$search("*.ALL;*") .nes. "" then delete/before=yesterday *.ALL;*
$       if f$search("''pname'.ALL") .nes. "" .and. vprint .nes. "" then delete-
                  'pname'.ALL;*
$!
$       i:=0
$       files := ""
$       nm := ""
$!
$ LOOP:
$       i=i+1
$       if 'i' .eq. 9 then goto LINKER
$       np = p'i'
$       err = np
$       if np .eqs. "" then goto LINKER
$       if f$length(np) .eq. f$locate(".",np)
$               then type := ".MAR"
$               else type = f$parse(np,,,"TYPE")
$       endif
$       np = f$parse(np,,,"NAME")
$!
$       if nm .nes. ""
$               then nm := 'nm''comma''np'
$               else nm = np
$       endif
$!
$       if type .eqs. ".MAR"
$               then asmbl := "YES"
$               else if type .eqs. ".obj"
$                       then asmbl := "NO"
$                       else write sys$output "*** ERROR ***"
$                            write sys$output err , " is not a valid filename."
$                            exit
$                       endif
$               endif
$!
$       if asmbl .eqs. "YES"
$       then
$               asm := "*** Assembling "'np'" ***"
$               write sys$output asm
$               macro/list'vdebug' 'np'+dname:valmacs/lib
$               lis := 'np'".lis"
$               lis := 'f$search(lis)
$               if files .nes. "" then files := 'files',dname:form.feed,
$               files := 'files''lis'
$       endif
$       goto LOOP              
$!
$! Link all routines
$!
$ LINKER:
$       linkfiles := "*** Linking "'m'" ***"
$       write sys$output linkfiles
$       link'vdebug' 'nm', dname:valprocs/lib
$!
$! Run executable program.
$!
$ RUNONLY:
$       if vrun .eqs. "" then goto DONE
$       write sys$output "*** Running "'pname'" ***"
$!
$! Assign files
$!
$       if vfile .nes. "" .and. dat .nes. "" then assign/nolog 'pname'.day sys$input
$       if vprint .nes. "" then assign/nolog 'pname'.out sys$output
$       if vfile .eqs. "" then assign nolog sys$command: sys$input:
$       if vdebug .nes. "" tehn assign/nolog sys$command dbg$input !sys$command
$!
$! Turn off abort on error.
$!
$       set noon
$!
$! Run the program
$!
$       run 'pname'
$!
$! Turn on abort on error.
$!
$       set on
$!
$! Deassign files.
$!
$       if vfile .nes. "" .and. dat .nes. "" then deassign sys$input
$       if vprint .nes. "" then deassign sys$output
$       if vfile .eqs. "" then deassign sys$input:
$       if vdebug .nes. "" then deassign dbg$input:
$!
$! Skip printing if not printing.
$!
$       if vprint .eqs "" then goto DONE
$!
$! Collect and print output.
$!
$       write sys$output "*** Printing ***"
$       out := 'pname'.OUT;*
$       out :='f$search(out)
$       if dat .nes. "" then files := 'files',dname:form.feed,'dat'
$       files := 'files',dname:form.feed
$       copy 'files' 'pname' .all/conc
$       if out .eqs. "" then goto OK
$       convert/append 'pname'.OUT 'pname'.ALL
$ OK:
$       print/notify 'pname'.ALL
$!
$! Display messages.
$!
$       write sys$output "Warning: Do not reuse VALRP woth this program until"
$       write sys$output "notified that its output has actually been printed."
$ DONE:
$       write sys$output " "
$       write sys$output "*** Done ***"
$       if vrun .nes. "" then goto EXIT
$       write sys$output " "
$       write sys$output "To run your program interactively, enter: RUNI "'pname'
$       write sys$output "To run your program with input file, enter: RUNR "'pname'
$ EXIT
$       write sys$output " "

