$!created on 10-MAR-2021 
$!---------------------- 
$! 
$! filename: TRASH_COLLECTOR.COM 
$! 
$!    input: none 
$!     idea: script executes at login and logout 
$!            searches for all bins, round robin trash grab 
$!          after review DELETED items are moved to main recycle dir 
$!            save items moved up one directory with added prefix 
$!      
$!     form: DCL array of bins -> grab file -> toggle 
$!            if 10 iterations or toggle not hit exit subprocedure 
$!            display loop -> type/page selection followed 
$!            Done - restructure filesystem 
$! 
$!     __________________________ 
$!    | Written by |K|O|13|13|S| | 
$!     ========================== 
$ 
$ display = "write sys$output" 
$ libcall = "***Logical Name of directory***:subroutine_library" 
$ trash = "" 
$ save = "" 
$ home_dir := "***your home directory***"
$ gosub get_bins 
$ gosub get_trash 
$ gosub list_trash 
$ gosub restruct 
$ exit 
$!get_bins procedure../  
$ 
$get_bins: 
$       i = -1 
$gb10:  i = i + 1 
$       bin'i = f$search("[" +''home_dir+"...]trash.dir") 
$       display bin'i
$       if bin'i .eqs. "" then goto gb19 
$       tmp = f$parse(bin'i,,,"directory") 
$       tmp = tmp - "]" 
$       tmp2 = f$parse(bin'i,,,"name")  
$       bin'i = tmp + "." + tmp2 + "]" 
$       goto gb10 
$  
$gb19: 
$       num_bins = i 
$       return 
$  
$!------------------------------------------------------------/.. 
$!get_trash procedure../ 
$ 
$get_trash: 
$!  
$       trash0 = "" 
$       toggle = 0 
$       k = 0  
$gt10:  
$       if (toggle) then goto gt19 !all bins are empty 
$       toggle = 1 
$       j = -1 
$gt15:  j = j + 1 
$       if k .eq. 10 then goto gt19 
$       if j .eq. num_bins then goto gt10  !all bins searched 
$       search = bin'j + "*.*;*" 
$       trash'k = f$search(search) 
$       if (trash'k .nes. "")  
$       then 
$               old = trash'k - f$parse(trash'k,,,"device") 
$               name = old - f$parse(old,,,"directory") - -
                             f$parse(old,,,"version") 
$               rename 'old ['home_dir.recycle]'name 
$               toggle = 0 
$               k = k + 1 
$        endif 
$        goto gt15 
$gt19: 
$       return 
$ 
$!-----------------------------------------------------------/.. 
$!list_trash procedure../ 
$list_trash: 
$10: 
$       i = -1  
$20:    i = i + 1 
$       terminate = "" 
$       if f$locate(i,save) .ne. f$length(save) then - 
$                               terminate = " -->save" 
$       if i .eq. 10 then goto 29 
$       if trash'i .eqs. "" then goto 29 
$       display i+1, ": ", trash'i, terminate 
$       old = trash'i - f$parse(trash'i,,,"device") 
$       name = old - f$parse(old,,,"directory") - -
                      f$parse(old,,,"version") 
$       goto 20 
$29: 
$       if i .eq. 0 then goto 39 
$30: 
$       libcall ask index S "ENTER NUMBER TO SAVE TRASH OR DONE:" -
                                 D U 
$       input_ok = ((f$integer(index) .lt. 11) .and. -
                     (f$integer(index) .ne. 0)) .or. -
                     (f$locate(index,"DONE") .eq. 0) 
$       if .not. input_ok then libcall signal rcy w errinput -
                                "Enter 1-10 or Done" 
$       if .not. input_ok then goto 30  
$       if f$locate(index,"DONE") .ne. 0 then goto 35 
$       goto 39 
$35: 
$       gosub review 
$       goto 10 
$39: 
$       return 
$!-------------------------------------------------------------/.. 
$!review procedure../ 
$review: 
$       libcall ask confirm B "would you like to review file?" "N" 
$       file = index - 1 
$       name = trash'file - f$parse(trash'file,,,"device") - -
                             f$parse(trash'file,,,"directory") - -
                             f$parse(trash'file,,,"version") 
$       if confirm then -
                 type/page ['home_dir.recycle]'name 
$       display ""
$       libcall ask confirm B "would you like to save file?" "Y" 
$       if confirm then save = save + f$string(file) 
$       return 
$!--------------------------------------------------------------/.. 
$!restruct procedure../ 
$ 
$restruct: 
$       i = -1 
$r10:   i = i + 1 
$       if i .eq. 10 then goto r19 
$       if f$locate (i,save) .ne. f$length(save) 
$       then 
$               folder = f$parse(trash'i,,,"directory") - ".TRASH" 
$               name = old - f$parse(old,,,"directory") - -
                              f$parse(old,,,"version") 
$               new = folder + "recovered_" + name 
$               rename ['home_dir.recycle]'name 'new 
$       endif 
$       goto r10 
$r19: 
$       gosub bin_check 
$       return 
$ 
$!-------------------------------------------------------------/.. 
$!bin_check procedure../ 
$  
$bin_check: 
$       checked_bins = "" 
$       i = -1 
$bt10:  i = i + 1 
$       if bin'i .eqs. "" then return 
$       curr = bin'i + "*.*;*" 
$       if f$search(curr) .eqs. "" 
$       then 
$               curr = bin'i - ".TRASH" + "TRASH.DIR;1" 
$               set security/prot=o:d 'curr 
$               delete 'curr 
$       endif 
$       goto bt10 
$ 
$!--------------------------------------------------------------/..  
$!get_bins doc../ 
$! Set Var:     bin(array), num_bins 
$! Synopsis:    makes an array with a simple search loop 
$!              uses fparse to format [Directory.bin] 
$!-------------------------------------------------------------/.. 
$!get_trash doc../ 
$! Set Var:     trash  
$! Synopsis:    double loop grabs one trash from each bin  
$!              in succession. Array up to 9. 
$!-------------------------------------------------------------/.. 
$!list_trash doc../ 
$! Set Val:     index 
$! Synopsis:    Displays all the trash and asks for review 
$!-----------------------------------------------------------/.. 
$!review doc../ 
$! Set Val:     save (string with index of files to be saved)  
$! Synopsis:    display? type/page current file. 
$!              save? add index to save.   
$!------------------------------------------------------------/.. 
$!restruct doc../ 
$! Synopsis:    Move saved files back to original folder w/  
$!              recovered prefix.  
$!              Files not saved remain in recycle folder  
$!              for recycling.  
$!-------------------------------------------------------------/.. 
$!bin_check doc../ 
$! Synopsis: Go through the bins and delete any empty ones 
$!-----------------------------------------------------------/..
