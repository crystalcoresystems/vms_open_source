$! From Writing real programs in DCL
$! By Steve Hoffman and Paul Anagnostopoulos
$
$       sublib__status = %x10000000
$       sublib__success = sublib__status + %x0001
$       on control_y then exit sublib__status + %004
$       on warning then exit $status .or. %x10000000
$
$       display = "write sys$output"
$       goto 'p1 
$!Ask Routine../ 
$ASK:
$
$       signal = "@" + f$environment("PROCEDURE") + " signal ask"
$       if  p3 .eqs. "B" .and p5 .nes. "" .and. -
            f$type(p5) .eqs. "INTEGER" then -
           p5 = f$element(p5,"/","NO/YES")
$       if p5 .nes. "" then p4 = f$extract(0,f$len(p4)-1,p4) + -
                      " [" + p5 + "]" + f$extract(f$len(p4)-1,1,p4)
$       if f$locate("S",p6) .ne. f$length(p6) then display "" 
$       if f$locate("H",p6) .ne. f$length(p6) then @'p7
$a10:   read sys$command /prompt="''p4 " input /end_of_file=a_eof
$       if input .eqs. "" then input = p5
$       input = f$edit(input,"TRIM")
$       if input .eqs. ""
$       then
$               signal w inputreq -
                      "Please enter a value; there is no default."
$       else if input .eqs. "?"
$       then
$               if p7 .nes. "" then @'p7
$               if p7 .eqs. "" then display -
                  "There is no help for this question."
$       else
$               goto a_'p3
$a_B:           input = f$edit(input,"UPCASE")
$               if f$locate(input,"YES") .eq. 0 .or. -
                   f$locate(input,"NO") .eq. 0
$               then
$                       input = f$integer(input)
$                       goto a19 
$               else
$                       signal w yesnoreq "Please answer YES or NO."
$               endif
$               goto a15
$ 
$a_I:           if f$type(input) .eqs."INTEGER"
$               then
$                       input = f$integer(input)
$                       goto a19
$               else
$                       signal w intreq "Input must be an integer."
$               endif
$               goto a15
$ 
$a_S:           if f$locate("U",p6) .ne. f$length(p6) then -
                        input = f$edit(input, "UPCASE")
$               goto a19
$a15:
$       endif
$       endif
$a_eof:
$       input = "^Z"
$       if f$locate("Z",p6) .ne. f$length(p6) then goto a19
$       signal i invctrlz "End-of-file is not a valid response."
$       goto a10
$a19:
$       'p2 == input
$       exit sublib__success
$!--------------------------------------------------------------/..
$!Lookup_Keyword Routine../
$LOOKUP_KEYWORD:
$ 
$       'p2 == ""
$       if p3 .eqs. "" then exit sublib__success
$       p3 = "," + f$edit(p3,"UPCASE")
$       p4 = "," + f$edit(p4,"UPCASE") + ","
$       p4_tail = f$extract(f$locate(p3,p4)+1, 999, p4)
$       if f$locate(p3,p4_tail) .eq. f$length(p4_tail) then -
                'p2 == f$element(0,",",p4_tail)
$       exit sublib__success
$!---------------------------------------------------------------/..
$!Signal Routine../
$SIGNAL:
$ 
$       prefix = f$fao("%!AS-!AS-!AS, ", p2, p3, p4)
$       i = 4
$s10:   i = i + 1
$       if i .gt. 8 then goto s19
$       if p'i .eqs. "" then goto s19
$       text = p'i
$       if f$type(text) .eqs. "INTEGER" then -
                text = f$message(text)
$       if f$ext(0,1,text) .nes. "%" then text = prefix + text
$       if i .gt. 5 then text[0,1] := "-"
$       display text
$       goto s10
$ 
$s19:
$       if p3 .eqs. "W" then p3 = "I"
$       exit sublib__status + f$locate(p3,"WSEIF")
$!-------------------------------------------------------------/..
$!Unique_Name Routine../
$UNIQUE_NAME:
$ 
$       if f$type(sublib_counter) .eqs. "" then sublib_counter == 0
$       sublib_counter == (sublib_counter+1) - -
                          (sublib_counter+1)/100*100
$       'p2 == f$fao("!AS!8AS!2ZL!AS", f$element(0,"?",p3), -
                        f$extract(12,11,f$time())-":"-":"-".", -
                        sublib_counter, f$element(1,"?",p3))
$       exit sublib__success
$!--------------------------------------------------------------/.. 
$!Verify_Symbol Routine../
$VERIFY_SYMBOL:
$ 
$       p3 = f$edit(p3,"TRIM,UPCASE")
$       if p4 .eqs. "" then p4 = 31
$       'p2 == f$fao("is limited to !UL characters", f$integer(p4))
$       if f$length(p3) .gt. p4 then exit sublib__succcess
$       v = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_" + p5
$       'p2 == ""
$       i = -1
$vs10:  i = i + 1
$       if i .ge. f$length(p3) then exit sublib__success
$       if f$locate(f$extract(i,1,p3),v) .lt. f$length(v) then -
                goto vs10
$       'p2 == f$fao("contains the invalid characcter ""!AS""", -
                f$extract(i,1,p3))
$       exit sublib__success
$!--------------------------------------------------------------/..              
$!Ask Doc../
$! 
$! Synopsis:    This subroutine asks the user a question and returns
$!              the answer. The prompt for the question is composed
$!              of a query string and optionally a default answer.
$! 
$! Parameters:  P2: A global symbol to recieve the answer
$!              P3: The data type of the answer. B for boolean
$!                  (yes/no); I for integer; S for string.
$!              P4: The query string for the question. It must end
$!                  with a punctuation character and no space.
$!              p5: The default answer (optional) if not specified
$!                  then an answer must be entered).
$!              P6: A comma-seperated list of options:
$!                      H: Display help before asking question.
$!                      S: Skip a line before asking question.
$!                      U: Upcase the input string.
$!                      Z: Allow CTRL/Z as answer.
$!              P7: The help specifier (optional). It must ne in
$!                  the form "procedure [parameter..]". The
$!                  procedure is invoked with the at-sign command.
$! 
$! Result:      For Boolean data type, a 0 (no) or 1 (yes). For
$!              Integer data type, the integer. For String data
$!              type, the string. If CTRL/Z is allowed and entered
$!              the string "^Z"
$!--------------------------------------------------------------/..
$!Lookup_Keyword Doc../
$!
$! Synopsis:    This subroutine looks up a keyword or its
$!              abbreviation in a list of keywords.
$!              If the keyword exists in the list and is
$!              unique, the full keyword is return.
$! 
$! Parameters:  P2: A global symbol to recieve the result.
$!              P3: The keyword or a unique abbreviation thereof.
$!              P4: A comma-seperated list of valid keywords.
$! 
$! Result:      If the (abbreviated) keyword is valid and unique
$!              the full keyword is returned. If the keyword is
$!              invalid or null, the null string is returned. 
$!--------------------------------------------------------------/..
$!Signal Doc../
$! 
$! Synopsis:    This subroutine "signals" a message, producing one
$!              or more message lines in the standard OpenVMS format
$!              It also exits with a status whose severity matches
$!              that of the message.
$! 
$! Parameters:  P2: The message facility code
$!              P3: The message severity (S, I, W, E or F).
$!              P4: The message identification.
$!              P5: The message text.
$!              Pn: optional message lines or status codes whose
$!                  corresponding message lines are to be included.
$! Status:      The severity of the exit status is equal to the
$!              message severity, except in the case of warnings.
$!              If the message severity is W, an informational
$!              severity is included in the status so that the
$!              caller's error handler is not invoked.
$!--------------------------------------------------------------/..
$!Unique_Name Doc../
$! 
$! Synopsis:    This subroutine generates a unique name suitable for
$!              use in creating a temporary file.
$! 
$! Parameters:  P2: The global symbol to recieve the result.
$!              P3: The pattern specifying the format of the unique
$!                  name. It must contain a question mark (?), which
$!                  is replaced with a unique number.
$! 
$! Result:      A unique name consisting of the pattern with the
$!              quesion mark replaced with a 10-digit number.
$!              The number is composed of 8 digits of time and a
$!              2-digit counter
$!--------------------------------------------------------------/.. 
$!Verify_Symbol Doc../
$! 
$! Synopsis:    This subroutine checks the validity of a string
$!              representing the name of a symbol. It checks the
$!              length and the individual characters.
$! 
$! Parameters:  P2: The global symbol to recieve the result.
$!              P3: The symbol to be checked.
$!              P4: Optional maximum length of symbol (default 31).
$!              P5: Optional valid characters in addition to the
$!                  standard letters, digits, dollar and underscore.
$! 
$! Result:      The null string if the symbol is okay. Otherwise a
$!              message fragment describing the error.
$!--------------------------------------------------------------/..
